#pragma once

#include "MessageHandler.h"
#include "NetworkHandler.h"

struct Server
{
	Server();

	std::string username;

	MessageHandler* messageHandler;

	NetworkHandler* networkHandler;

	Message top;

	static Server* instance;
};