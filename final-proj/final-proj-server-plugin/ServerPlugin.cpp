#include "ServerPlugin.h"
#include "Server.h"

UNITY_API void Initialize(unsigned short port, unsigned int maximumConnections)
{
	if (!Server::instance)
	{
		Server::instance = new Server();

		NetworkHandlerInfo networkInfo;
		networkInfo.isHost = true;
		networkInfo.address = "127.0.0.1";
		networkInfo.localPort = port;
		networkInfo.remotePort = 0;
		networkInfo.maximumConnections = maximumConnections;
		networkInfo.priorityMode = HIGH_PRIORITY;
		networkInfo.reliabilityMode = RELIABLE_ORDERED;
		Server::instance->networkHandler = new NetworkHandler(networkInfo);

		MessageHandlerInfo messengerInfo;
		messengerInfo.maximumMessagesPerFrame = 60;
		messengerInfo.network = Server::instance->networkHandler;
		Server::instance->messageHandler = new MessageHandler(messengerInfo);
	}
}

UNITY_API void Terminate()
{
	delete Server::instance->networkHandler;
	delete Server::instance->messageHandler;
	delete Server::instance;
}

UNITY_API void SetUsername(char* username)
{
	Server::instance->username = username;
}

UNITY_API void GetInbound()
{
	Server::instance->messageHandler->GetInbound();
}

UNITY_API void SendOutbound()
{
	Server::instance->messageHandler->SendOutbound();
}

UNITY_API void Send(int type, char* data)
{
	MessageInfo messageInfo;
	messageInfo.type = (MessageType)type;
	messageInfo.data = std::string(data);
	messageInfo.time = Server::instance->networkHandler->Time();

	Message message(messageInfo);
	Server::instance->messageHandler->Write(message);
}

UNITY_API bool Next()
{
	Server* server = Server::instance;
	if (auto message = server->messageHandler->Read(); message.has_value())
	{
		server->top = message.value();
		server->messageHandler->Write(server->top);
		return true;
	}

	return false;
}

UNITY_API int GetType()
{
	return (int)Server::instance->top.Type();
}

UNITY_API char* GetData()
{
	return (char*)Server::instance->top.Data();
}

UNITY_API double GetTimestamp()
{
	return Server::instance->top.Time();
}

UNITY_API int NetworkState()
{
	return (int)Server::instance->networkHandler->StartupResult();
}