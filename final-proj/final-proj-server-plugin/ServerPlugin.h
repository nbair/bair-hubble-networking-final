#pragma once

#define UNITY_API __declspec(dllexport)

extern "C"
{
	// INIT

	UNITY_API void Initialize(unsigned short port, unsigned int maximumConnections);

	UNITY_API void Terminate();

	UNITY_API void SetUsername(char* username);

	// LOOP

	UNITY_API void GetInbound();

	UNITY_API void SendOutbound();

	// SEND

	UNITY_API void Send(int type, char* data);

	// RECEIVE

	UNITY_API bool Next();

	UNITY_API int GetType();

	UNITY_API char* GetData();

	UNITY_API double GetTimestamp();

	// QUERY

	UNITY_API int NetworkState();
}