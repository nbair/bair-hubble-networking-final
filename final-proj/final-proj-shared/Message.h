#pragma once

#include <string>

#include "RakNet/BitStream.h"
#include "RakNet/GetTime.h"
#include "RakNet/MessageIdentifiers.h"

enum MessageType
{
	ID_MESSAGE_EMPTY = ID_USER_PACKET_ENUM,
	ID_MESSAGE_WELCOME,
	ID_MESSAGE_CHAT,
	ID_MESSAGE_PROMPT,
	ID_MESSAGE_CLIENT_RESPONSE,
	ID_MESSAGE_CZAR_CHOICE,
	ID_MESSAGE_BOUNCE,
	ID_MESSAGE_CZAR_SELECT,
	ID_MESSAGE_GAME_END,
	ID_MESSAGE_ROUND_WON,
	ID_MESSAGE_SWAP_STATE
};

struct MessageInfo
{
	MessageType type;

	std::string data;

	double time;
};

class Message
{
	public:

		Message();

		Message(MessageInfo info);

		Message(const Message& other);

		MessageType Type() const;

		const char* Data() const;

		double Time() const;

		friend inline bool operator< (const Message lhs, const Message rhs)
		{
			return lhs.Time() < rhs.Time();
		}

	private:

		MessageType type;	

		std::string data;

		double time;
};