#include "NetworkHandler.h"

NetworkHandler::NetworkHandler(NetworkHandlerInfo info)
{	
	peer = RakNet::RakPeerInterface::GetInstance();
	remotePort = info.remotePort;
	address = info.address;

	priorityMode = info.priorityMode;
	reliabilityMode = info.reliabilityMode;

	if (info.isHost)
	{
		RakNet::SocketDescriptor socket(info.localPort, nullptr);
		startupResult = peer->Startup(info.maximumConnections, &socket, 1);
	}
	else
	{
		RakNet::SocketDescriptor socket(info.localPort, nullptr);
		startupResult = peer->Startup(info.maximumConnections, &socket, 1);
	}
	
	peer->SetMaximumIncomingConnections(info.maximumConnections);
	peer->SetOccasionalPing(true);
}

NetworkHandler::~NetworkHandler()
{
	if (connected)
	{
		Disconnect();
	}

	RakNet::RakPeerInterface::DestroyInstance(peer);
}

void NetworkHandler::Connect()
{
	connectionResult = peer->Connect(address.c_str(), remotePort, nullptr, 0);
}

void NetworkHandler::Disconnect()
{
	peer->Shutdown(0);
}

void NetworkHandler::Send(Message message)
{
	auto out = std::make_unique<RakNet::BitStream>();
	out->Write(message.Type());
	out->Write(message.Data());
	out->Write(message.Time());

	peer->Send(out.get(), priorityMode, reliabilityMode, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void NetworkHandler::Send(Message message, RakNet::SystemAddress to)
{
	auto out = std::make_unique<RakNet::BitStream>();
	out->Write(message.Time());
	out->Write(message.Type());
	out->Write(message.Data());

	peer->Send(out.get(), priorityMode, reliabilityMode, 0, to, false);
}

std::optional<Message> NetworkHandler::Receive()
{
	if (auto packet = peer->Receive(); packet)
	{
		MessageInfo info;
		auto in = std::make_unique<RakNet::BitStream>(packet->data, packet->length, false);	
		in->Read(info.type);

		switch (info.type)
		{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
				info.data = "Another client has disconnected";
				break;

			case ID_REMOTE_CONNECTION_LOST:
				info.data = "Another client has lost the connection";
				break;

			case ID_REMOTE_NEW_INCOMING_CONNECTION:
				info.data = "Another client has connected";
				break;

			case ID_CONNECTION_REQUEST_ACCEPTED:
				info.data = "Our connection request has been accepted";
				break;

			case ID_NEW_INCOMING_CONNECTION:
				info.data = "A new connection is incoming";
				break;

			case ID_NO_FREE_INCOMING_CONNECTIONS:
				info.data = "The server is full";
				break;

			case ID_DISCONNECTION_NOTIFICATION:
				info.data = "A connection was disconnected";
				break;

			case ID_CONNECTION_LOST:
				info.data = "A connection was lost";
				break;

			case ID_MESSAGE_EMPTY:
				info.data = "Empty message";
				in->Read(info.time);
				break;

			case ID_MESSAGE_WELCOME:	
			case ID_MESSAGE_CHAT:
			case ID_MESSAGE_PROMPT:
			case ID_MESSAGE_CLIENT_RESPONSE:
			case ID_MESSAGE_CZAR_CHOICE:
			case ID_MESSAGE_BOUNCE:
			case ID_MESSAGE_CZAR_SELECT:
			case ID_MESSAGE_GAME_END:
			case ID_MESSAGE_ROUND_WON:
			case ID_MESSAGE_SWAP_STATE:
				in->Read(info.data);
				in->Read(info.time);
				break;

			default: return std::nullopt;
		}

		peer->DeallocatePacket(packet);

		return Message(info);
	}
	
	return std::nullopt;
}

double NetworkHandler::Time() const
{
	return (double)RakNet::GetTime();
}

const RakNet::StartupResult NetworkHandler::StartupResult() const
{
	return startupResult;
}

const RakNet::ConnectionAttemptResult NetworkHandler::ConnectionResult() const
{
	return connectionResult;
}