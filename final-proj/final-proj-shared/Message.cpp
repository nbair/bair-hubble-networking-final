#include "Message.h"

#include <sstream>

Message::Message() :
	type(MessageType::ID_MESSAGE_EMPTY),
	data(""),
	time(0.0)
{}

Message::Message(MessageInfo info) :
	type(info.type), 
	data(info.data), 
	time(info.time)
{}

Message::Message(const Message& other) :
	type(other.type),
	data(other.data),
	time(other.time)
{}

MessageType Message::Type() const
{
	return type;
}

const char* Message::Data() const
{
	return data.c_str();
}

double Message::Time() const
{
	return time;
}