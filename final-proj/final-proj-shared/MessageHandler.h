#pragma once

#include <queue>
#include <string>
#include <memory>

#include "NetworkHandler.h"

struct MessageHandlerInfo
{
	NetworkHandler* network;

	unsigned int maximumMessagesPerFrame;
};

class MessageHandler
{
	public:

		MessageHandler(MessageHandlerInfo info);

		~MessageHandler();

		void GetInbound();

		void SendOutbound();

		void Write(Message message);

		std::optional<Message> Read();

	private:

		NetworkHandler* network;

		unsigned int maximumMessagesPerFrame;

		std::priority_queue<Message> inbound;

		std::priority_queue<Message> outbound;
};