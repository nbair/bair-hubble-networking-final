#include "MessageHandler.h"

MessageHandler::MessageHandler(MessageHandlerInfo info) :
	network(info.network),
	maximumMessagesPerFrame(info.maximumMessagesPerFrame)
{}

MessageHandler::~MessageHandler()
{}

void MessageHandler::GetInbound()
{
	unsigned int processedThisFrame = 0;
	while (true)
	{
		if (auto message = network->Receive(); message.has_value() && processedThisFrame < maximumMessagesPerFrame)
		{
			inbound.push(message.value());
			++processedThisFrame;
			continue;
		}

		break;
	}
}

void MessageHandler::SendOutbound()
{
	while (!outbound.empty())
	{
		network->Send(outbound.top());
		outbound.pop();
	}
}

void MessageHandler::Write(Message message)
{
	outbound.push(message);
}

std::optional<Message> MessageHandler::Read()
{
	if (!inbound.empty())
	{
		auto top = inbound.top();
		inbound.pop();
		return top;
	}

	return std::nullopt;
}