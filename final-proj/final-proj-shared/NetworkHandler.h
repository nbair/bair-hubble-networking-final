#pragma once

#include <memory>
#include <string>
#include <optional>

#include "RakNet/RakPeerInterface.h"

#include "Message.h"

struct NetworkHandlerInfo
{
	bool isHost;

	unsigned short remotePort;

	unsigned short localPort;

	unsigned int maximumConnections;

	std::string address;

	PacketPriority priorityMode;

	PacketReliability reliabilityMode;
};

class NetworkHandler
{
	public:

		NetworkHandler(NetworkHandlerInfo info);

		~NetworkHandler();

		void Connect();

		void Disconnect();

		void Send(Message message);

		void Send(Message message, RakNet::SystemAddress to);

		std::optional<Message> Receive();

		double Time() const;

		const RakNet::StartupResult StartupResult() const;

		const RakNet::ConnectionAttemptResult ConnectionResult() const;

	private:

		RakNet::RakPeerInterface* peer;

		std::string address;

		unsigned short remotePort;

		PacketPriority priorityMode;

		PacketReliability reliabilityMode;

		RakNet::StartupResult startupResult;

		RakNet::ConnectionAttemptResult connectionResult;

		bool connected;
};