#include "ClientPlugin.h"
#include "Client.h"

UNITY_API void Initialize(char* address, unsigned short port)
{
	if (!Client::instance)
	{
		Client::instance = new Client();

		NetworkHandlerInfo networkInfo;
		networkInfo.isHost = false;
		networkInfo.address = address;
		networkInfo.localPort = 0;
		networkInfo.remotePort = port;
		networkInfo.maximumConnections = 1;
		networkInfo.priorityMode = HIGH_PRIORITY;
		networkInfo.reliabilityMode = RELIABLE_ORDERED;
		Client::instance->networkHandler = new NetworkHandler(networkInfo);

		MessageHandlerInfo messengerInfo;
		messengerInfo.maximumMessagesPerFrame = 30;
		messengerInfo.network = Client::instance->networkHandler;
		Client::instance->messageHandler = new MessageHandler(messengerInfo);
	}
}

UNITY_API void Terminate()
{
	delete Client::instance->networkHandler;
	delete Client::instance->messageHandler;
	delete Client::instance;
}

UNITY_API void SetUsername(char* username)
{
	Client::instance->username = username;
}

UNITY_API void Connect()
{
	Client::instance->networkHandler->Connect();
}

UNITY_API void Disconnect()
{
	Client::instance->networkHandler->Disconnect();
}

UNITY_API void GetInbound()
{
	Client::instance->messageHandler->GetInbound();
}

UNITY_API void SendOutbound()
{
	Client::instance->messageHandler->SendOutbound();
}

UNITY_API void Send(int type, char* data)
{
	MessageInfo messageInfo;
	messageInfo.type = (MessageType)type;
	messageInfo.data = std::string(data);
	messageInfo.time = Client::instance->networkHandler->Time();

	Message message(messageInfo);
	Client::instance->messageHandler->Write(message);
}

UNITY_API bool Next()
{
	Client* client = Client::instance;
	if (auto message = client->messageHandler->Read(); message.has_value())
	{
		client->top = message.value();
		return true;
	}

	return false;
}

UNITY_API int GetType()
{
	return (int)Client::instance->top.Type();
}

UNITY_API char* GetData()
{
	return (char*)Client::instance->top.Data();
}

UNITY_API double GetTimestamp()
{
	return Client::instance->top.Time();
}

UNITY_API int NetworkState()
{
	return (int)Client::instance->networkHandler->StartupResult();
}

UNITY_API int ConnectionState()
{
	return (int)Client::instance->networkHandler->ConnectionResult();
}