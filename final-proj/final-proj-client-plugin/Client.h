#pragma once

#include "MessageHandler.h"
#include "NetworkHandler.h"

#include <memory>

struct Client
{
	Client();

	std::string username;

	MessageHandler* messageHandler;

	NetworkHandler* networkHandler;

	Message top;

	static Client* instance;
};