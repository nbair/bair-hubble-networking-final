#pragma once

#define UNITY_API __declspec(dllexport)

extern "C"
{
	// INIT

	UNITY_API void Initialize(char* address, unsigned short port);

	UNITY_API void Terminate();

	UNITY_API void SetUsername(char* username);

	UNITY_API void Connect();

	UNITY_API void Disconnect();

	// LOOP

	UNITY_API void GetInbound();

	UNITY_API void SendOutbound();

	// SEND

	UNITY_API void Send(int type, char* data);

	// RECEIVE

	UNITY_API bool Next();

	UNITY_API int GetType();

	UNITY_API char* GetData();

	UNITY_API double GetTimestamp();

	// QUERY

	UNITY_API int NetworkState();

	UNITY_API int ConnectionState();
}