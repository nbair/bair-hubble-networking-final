﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

public static class Networking
{
    [DllImport("final-proj-server-plugin")]
    public static extern void Initialize(short port, int maximumConnections);

    [DllImport("final-proj-server-plugin")]
    public static extern void Terminate();

    [DllImport("final-proj-server-plugin")]
    public static extern void SetUsername(string username);

    [DllImport("final-proj-server-plugin")]
    public static extern void GetInbound();

    [DllImport("final-proj-server-plugin")]
    public static extern void SendOutbound();

    [DllImport("final-proj-server-plugin")]
    public static extern void Send(int type, string data);

    [DllImport("final-proj-server-plugin")]
    public static extern bool Next();

    [DllImport("final-proj-server-plugin")]
    public static extern new int GetType();

    [DllImport("final-proj-server-plugin")]
    public static extern string GetData();

    [DllImport("final-proj-server-plugin")]
    public static extern double GetTimestamp();

    [DllImport("final-proj-server-plugin")]
    public static extern int NetworkState();
}