﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PluginConnector : MonoBehaviour
{
    enum MESSAGES
    {
        EMPTY = 134,
        WELCOME = 135,
        CHATMESSAGE = 136,
        DISPLAYPROMPT = 137,
        RESPONCEPLAYER = 138,
        CZARCHOICE = 139,
        BOUNCETHATBOI = 140,
        CZARCHOSEN = 141,
        GAMEEND = 142,
        ROUNDWINNER = 143,
        SWAPSTATE = 144,
        COUNT
    }

    [SerializeField]
    ServerGameplayManager gm;

    private void Update()
    {
        PollForMessage();
    }

    void PollForMessage()
    {
        //Networking get message & get type

        if(Networking.Next())
        {
            MESSAGES messageType = (MESSAGES)Networking.GetType();
            string packetMessage = Networking.GetData();
            string[] messageOut;

            switch (messageType)
            {

                //Routing messages for the clients, there is no info to prosses here we just need it to be broadcased to the players
                case MESSAGES.CHATMESSAGE: //Recived a chat message
                    SendChatMessage(packetMessage);
                    break;

                case MESSAGES.BOUNCETHATBOI: //Someone bounced a boi
                    SendBallBounce(packetMessage);
                    break;

                // ---------------------------------Info used by the server--------------------------------------------------------


                case MESSAGES.WELCOME: //Connection
                        //A user connected
                    messageOut = InterpretMessage(packetMessage);
                    gm.AddPlayer(messageOut[0]);
                    break;

                case MESSAGES.RESPONCEPLAYER: //Recives a responce from a player 
                    SendUserResponce(packetMessage);
                    messageOut = InterpretMessage(packetMessage);
                    gm.ReciveResponce(messageOut[1], messageOut[0]);
                    break;

                case MESSAGES.CZARCHOICE: //The czar's winning choice
                    messageOut = InterpretMessage(packetMessage);
                    string winningUser = gm.GetUserFromResponce(messageOut[0]);
                    SendCzarChoice(winningUser);
                    gm.AddPlayerPoints(winningUser);

                    if (gm.CheckForEnd())
                    {
                        //End the game
                        gm.gamestate = ServerGameplayManager.SERVERGAMESTATE.ENDOFGAME;
                    }
                    else
                    {
                        //Setup a new round
                        gm.SetupNextPrompt();
                    }

                    break;
            }
        }


    }

    string[] InterpretMessage(string theMessage)
    {
        string[] output = theMessage.Split('|');

        return output;
    }

    void SendUserResponce(string theResponce)
    {
        Networking.Send((int)MESSAGES.RESPONCEPLAYER, theResponce);
    }

    void SendChatMessage(string theChat)
    {
        Networking.Send((int)MESSAGES.CHATMESSAGE, theChat);
    }

    void SendBallBounce(string message)
    {
        Networking.Send((int)MESSAGES.BOUNCETHATBOI, message);
    }

    void SendCzarChoice(string winningUser)
    {
        Networking.Send((int)MESSAGES.ROUNDWINNER, winningUser);
    }

    //Messages sent by the GM -------------------------------------

    public void SendPrompt(string prompt)
    {
        //Tell the plugin to sendthe prompt to the users
        Networking.Send((int)MESSAGES.DISPLAYPROMPT, prompt);
    }

    public void ChooseCzar(string name, string prompt)
    {
        //Tell the plugin to send a czar chosen packet to the czar, and a prompt message to the players
        Networking.Send((int)MESSAGES.CZARCHOSEN, name + "|" + prompt);
    }

}
