﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ServerGameplayManager : MonoBehaviour
{

    public struct PlayerData
    {
        public string playerResponce; //Holds the player responce
        public string playerName; //To make sure we constantly send info to the correct player (This will need to be reflected in the networking plugin
        public int playerPoints;
    } //END OF PLAYER STRUCT

    //------------------------------------------------------------------------------------------------

    //Mostly for my own sanity when checking what state everything is in
    public enum SERVERGAMESTATE
    {
        NULL = -1,

        MENU,
        WAITINGFORPLAYERS,
        WAITINGFORRESPONCES,
        WAITINGFORCZARCHOICE,
        ENDOFGAME,

        COUNT //# of states
    }

    [SerializeField]
    int maxPlayers = 4;

    [SerializeField]
    int poinLimit = 3;

    [SerializeField]
    List<string> promptList = new List<string>();

    [SerializeField]
    TextMeshProUGUI promptDisplay;

    [SerializeField]
    List<TextMeshProUGUI> playerResponceDisplays = new List<TextMeshProUGUI>();

    [SerializeField]
    Canvas waitingCanvas;

    [SerializeField]
    Canvas gameCanavas;

    [SerializeField]
    Canvas endOfGameCanvas;

    [HideInInspector]
    public SERVERGAMESTATE gamestate;
    //List<PlayerData> playerList = new List<PlayerData>();
    Dictionary<string, PlayerData> playerMap = new Dictionary<string, PlayerData>();
    List<string> playerMapKeys = new List<string>();
    int currentCzarPlayerID;
    int highestID = 0;
    [SerializeField]
    PluginConnector pluginConnector;


    private void Update()
    {
        switch(gamestate)
        {
            case SERVERGAMESTATE.WAITINGFORPLAYERS:
                if (playerMap.Count >= maxPlayers)
                {
                    gamestate = SERVERGAMESTATE.WAITINGFORRESPONCES;
                    waitingCanvas.enabled = false;
                    gameCanavas.enabled = true;
                    endOfGameCanvas.enabled = false;
                    SetupNextPrompt();
                }
                break;

            case SERVERGAMESTATE.WAITINGFORRESPONCES:
                DisplayResponces(); //Keep displaying the responces
                break;

            case SERVERGAMESTATE.ENDOFGAME:
                waitingCanvas.enabled = false;
                gameCanavas.enabled = false;
                endOfGameCanvas.enabled = true;
                break;
        }
    }

    public void AddPlayer(string inBoundPlayerData) //Call this in plugin connector when a player info inbounds
    {
        PlayerData newPlayer = new PlayerData();
        newPlayer.playerName = inBoundPlayerData;
        newPlayer.playerPoints = 0;
        newPlayer.playerResponce = "???";
        if(newPlayer.playerName != "") //There was an inbound player
        {
            playerMap.Add(newPlayer.playerName, newPlayer);
            playerMapKeys.Add(newPlayer.playerName);
        }
    }

    void DisplayResponces()
    {
        int i = 0;
        foreach(KeyValuePair<string, PlayerData> keyPair in playerMap)
        {
            playerResponceDisplays[i].text = keyPair.Value.playerResponce;
            i++;
        }
    }

    //--------------------------------Public functions--------------------------------

    public void ReciveResponce(string responce, string userID)
    {
        //Update the responce 
        PlayerData pd = playerMap[userID];
        pd.playerResponce = responce;
        playerMap[userID] = pd;
    }

    public string GetUserFromResponce(string responce)
    {
        foreach(KeyValuePair<string, PlayerData> keyPair in playerMap)
        {
            if (keyPair.Value.playerResponce == responce)
                return keyPair.Key;
        }

        return "NULL";
    }

    public void SetupNextPrompt()
    {
        gamestate = SERVERGAMESTATE.WAITINGFORRESPONCES;

        string randomPrompt = promptList[Random.Range(0, promptList.Count - 1)];

        pluginConnector.ChooseCzar(playerMapKeys[Random.Range(0, playerMapKeys.Count - 1)],
            randomPrompt); //Choose the next czar and send a message

        pluginConnector.SendPrompt(randomPrompt);

        promptDisplay.text = randomPrompt;

        //Wipe the previous responces from the players
        foreach (KeyValuePair<string, PlayerData> keyPair in playerMap)
        {
            PlayerData pd = playerMap[keyPair.Key];
            pd.playerResponce = "???";
            playerMap[keyPair.Key] = pd;
        }

    }

    public void AddPlayerPoints(string player)
    {
        PlayerData pd = playerMap[player];
        pd.playerPoints++;
        playerMap[player] = pd;
    }

    public bool CheckForEnd()
    {
        foreach (KeyValuePair<string, PlayerData> keyPair in playerMap)
        {
            if (keyPair.Value.playerPoints >= poinLimit)
                return true;
        }

        return false;
    }
}
