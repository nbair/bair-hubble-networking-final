﻿using UnityEngine;

public class Test : MonoBehaviour
{
    private void Awake()
    {
        Networking.Initialize(2000, 10);
       
        Debug.Log($"Network state: {Networking.NetworkState()}");

        Networking.SetUsername("Server");
    }

    private void OnDestroy()
    {
        Networking.Terminate();
    }

    private void Update()
    {
        Networking.GetInbound();
        while (Networking.Next())
        {
            int type = Networking.GetType();
            string data = Networking.GetData();
            double time = Networking.GetTimestamp();

            Debug.Log($"Message | Type: {type} Data: {data} Time: {time}");
        }
    }

    private void LateUpdate()
    {
        Networking.SendOutbound();
    }
}