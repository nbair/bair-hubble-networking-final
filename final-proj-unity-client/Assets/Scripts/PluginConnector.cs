﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PluginConnector : MonoBehaviour
{
    /*  These are the states that the program can be in, this is particuarly usefull if the server needs to change the state that the clients are in
     *  EXAMPLE: The game ends, so the server changes the game state to END, this will tell the manager to stop the loop
     *  
     *  We can also gate what info is asked for based on the state being recived, this way we arn't pining the plugin every frame for every peice of info
     *  EXAMPLE: If the state is set to PICKING, the Client will then poll for a prompt then
     */
    public enum GAMESTATE
    {
        NULL = -1,

        //In Game States
        WAITING_PLAYER, //The client is waiting for input
        WAITING_CZAR, //The client is waiting for input as the czar
        PLAYER, //The client is picking a prompt
        CZAR, //The client is picking a responce to win

        //Menu / Connection states
        END, //The game has ended, waiting for server input
        CONNECTING, //The client is attempting to connect to the server
        CONNECTED, //The client is waiting to join a game but has connected
        MENU, //The client is in a menu

        COUNT //# of states
    }

    enum MESSAGES
    {
        EMPTY = 134,
        WELCOME = 135,
        CHATMESSAGE = 136,
        DISPLAYPROMPT = 137,
        RESPONCEPLAYER = 138,
        CZARCHOICE = 139,
        BOUNCETHATBOI = 140,
        CZARCHOSEN = 141,
        GAMEEND = 142,
        ROUNDWINNER = 143,
        SWAPSTATE = 144,
        COUNT
    }

    [SerializeField]
    GameplayManager gm;

    [SerializeField]
    ChatBox cb;

    [SerializeField]

    private void Update()
    {
        PollForMessage();
    }

    void PollForMessage()
    {
        //Networking get message & get type

        if(Networking.Next())
        {
            MESSAGES messageType = (MESSAGES)Networking.GetType();
            string packetMessage = Networking.GetData();
            string[] messageOut;

            switch (messageType)
            {
                case MESSAGES.CHATMESSAGE: //Recived a chat message
                    messageOut = InterpretMessage(packetMessage);
                    cb.ReciveChat(messageOut[0] + ": " + messageOut[1]);
                    break;

                case MESSAGES.DISPLAYPROMPT: //Recive a prompt to display
                    messageOut = InterpretMessage(packetMessage);
                    gm.RecivePromptToDisplay(messageOut[0]);
                    break;
                case MESSAGES.RESPONCEPLAYER: //Recived a player responce
                    messageOut = InterpretMessage(packetMessage);
                    gm.DisplayCzarChoice(messageOut[0]);
                    break;

                case MESSAGES.BOUNCETHATBOI: //Someone bounced a boi
                    messageOut = InterpretMessage(packetMessage);
                    gm.BounceBall(messageOut[0]);
                    break;

                case MESSAGES.CZARCHOSEN: //A czar has been chosen
                    messageOut = InterpretMessage(packetMessage);
                    if (messageOut[0] == gm.userName)
                        gm.UpdateState(GAMESTATE.CZAR);
                    else
                        gm.UpdateState(GAMESTATE.PLAYER);
                    break;

                case MESSAGES.ROUNDWINNER: //The czar recives a choice (USER NAME)
                    messageOut = InterpretMessage(packetMessage);
                    if (messageOut[0] == gm.userName)
                        gm.RecivePoints();
                    break;

                case MESSAGES.SWAPSTATE: //Swap State
                    messageOut = InterpretMessage(packetMessage);
                    gm.UpdateState(ConvertStringToGAMESTATE(messageOut[0]));
                    break;
            }
        }


    }

    string[] InterpretMessage(string theMessage)
    {
        string[] output = theMessage.Split('|');

        return output;
    }

    GAMESTATE ConvertStringToGAMESTATE(string theString)
    {
        GAMESTATE output = GAMESTATE.NULL;

        if (theString == "WAITING_PLAYER")
            output = GAMESTATE.WAITING_PLAYER;
        else if (theString == "WAITING_CZAR")
            output = GAMESTATE.WAITING_CZAR;
        else if (theString == "PLAYER")
            output = GAMESTATE.PLAYER;
        else if (theString == "CZAR")
            output = GAMESTATE.CZAR;
        else if (theString == "END")
            output = GAMESTATE.END;
        else if (theString == "CONNECTING")
            output = GAMESTATE.CONNECTING;
        else if (theString == "CONNECTED")
            output = GAMESTATE.CONNECTED;
        else if (theString == "MENU")
            output = GAMESTATE.MENU;

        return output; 
    }

    public bool SendResponce(string prompt, string userName) //Sends the responce to the server
    {
        string output = userName + "|" + prompt;
        //Send output
        Networking.Send((int)MESSAGES.RESPONCEPLAYER, output);
        return false; //Returns false if fail
    }

    void SendState(GAMESTATE stateOUT, string user) //Sends the state to the server (May not be needed for the client)
    {
        
    }

    public void SendCzarChoice(string choice, string userName)
    {
        string output = userName + "|" + choice;
        Networking.Send((int)MESSAGES.CZARCHOICE, choice);
    }

    public void ConnectToIP(string IP)
    {
        Networking.Connect();
    }
    
    public void SendChatMessage(string theMessage, string userName)
    {
        Networking.Send((int)MESSAGES.CHATMESSAGE, userName + "|" + theMessage);
    }
}
