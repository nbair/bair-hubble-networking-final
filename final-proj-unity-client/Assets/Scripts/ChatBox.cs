﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChatBox : MonoBehaviour
{
    [SerializeField]
    List<TextMeshProUGUI> chatOutput = new List<TextMeshProUGUI>();

    [SerializeField]
    InputField chatText;

    [SerializeField]
    PluginConnector pluginConnector;

    [SerializeField]
    GameplayManager gameplayManager;

    public void ReciveChat(string theChat)
    {
        chatOutput[4].text = chatOutput[3].text;
        chatOutput[3].text = chatOutput[2].text;
        chatOutput[2].text = chatOutput[1].text;
        chatOutput[1].text = chatOutput[0].text;
        chatOutput[0].text = theChat;
    }

    public void SendChat()
    {
        pluginConnector.SendChatMessage(chatText.text, gameplayManager.userName);
    }
}
