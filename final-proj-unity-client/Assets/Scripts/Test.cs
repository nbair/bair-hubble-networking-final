﻿using UnityEngine;

public class Test : MonoBehaviour
{
    [SerializeField]
    private string address;

    [SerializeField]
    private string username;

    private void Awake()
    {
        Networking.Initialize(address, 2000);

        Debug.Log($"Network state: {Networking.NetworkState()}");

        Networking.SetUsername(username);
    }

    private void Start()
    {
        Networking.Connect();

        Debug.Log($"Connection state: {Networking.ConnectionState()}");

        Networking.Send(135, "Hello World!");
    }

    private void OnDestroy()
    {
        Networking.Disconnect();
        Networking.Terminate();
    }

    private void Update()
    {
        Networking.GetInbound();
        while (Networking.Next())
        {
            int type = Networking.GetType();
            string data = Networking.GetData();
            double time = Networking.GetTimestamp();

            Debug.Log($"Message | Type: {type} Data: {data} Time: {time}");
        }
    }

    private void LateUpdate()
    {
        Networking.SendOutbound();
    }
}