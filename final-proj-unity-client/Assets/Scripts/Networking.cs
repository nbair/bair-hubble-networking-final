﻿using System.Runtime.InteropServices;

public static class Networking
{
    [DllImport("final-proj-client-plugin")]
    public static extern void Initialize(string address, short port);

    [DllImport("final-proj-client-plugin")]
    public static extern void Terminate();

    [DllImport("final-proj-client-plugin")]
    public static extern void SetUsername(string username);

    [DllImport("final-proj-client-plugin")]
    public static extern void Connect();

    [DllImport("final-proj-client-plugin")]
    public static extern void Disconnect();

    [DllImport("final-proj-client-plugin")]
    public static extern void GetInbound();

    [DllImport("final-proj-client-plugin")]
    public static extern void SendOutbound();

    [DllImport("final-proj-client-plugin")]
    public static extern void Send(int type, string data);

    [DllImport("final-proj-client-plugin")]
    public static extern bool Next();

    [DllImport("final-proj-client-plugin")]
    public static extern new int GetType();

    [DllImport("final-proj-client-plugin")]
    public static extern string GetData();

    [DllImport("final-proj-client-plugin")]
    public static extern double GetTimestamp();

    [DllImport("final-proj-client-plugin")]
    public static extern int NetworkState();

    [DllImport("final-proj-client-plugin")]
    public static extern int ConnectionState();
}