﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBounce : MonoBehaviour
{
    
    Rigidbody2D rb;

    [SerializeField]
    float pushPower = 1f;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    public void BounceThatBoi()
    {
        rb.AddForce(Vector2.up * pushPower);
    }
}
