﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    PluginConnector.GAMESTATE currentState = PluginConnector.GAMESTATE.MENU; //Set to menu by default so 
    PluginConnector.GAMESTATE prevState = PluginConnector.GAMESTATE.NULL;

    [SerializeField]
    PluginConnector pluginConnector;

    [Header("Various canvas needed")]

    [SerializeField]
    CzarCanvasDataHolder czarHolder;

    [SerializeField]
    PlayerCanvasDataHolder playerHolder;

    [SerializeField]
    Canvas mainMenuCanvas;

    [SerializeField]
    Canvas wantToConnectCanvas;

    [SerializeField]
    InputField userNameInput;

    List<Canvas> allCanvas = new List<Canvas>(); //A list of all the canvas so I can easily turn them on & off
    List<string> inputedResponces = new List<string>(); //A list of all the responces inputed
    string responce;

    [SerializeField]
    List<BallBounce> balls;

    public string userName;
    bool connected = false;

    int points;

    // Start is called before the first frame update
    void Start()
    {
        allCanvas.Add(czarHolder.czarCanvas);
        allCanvas.Add(playerHolder.playerCanvas);
        allCanvas.Add(mainMenuCanvas);
        allCanvas.Add(wantToConnectCanvas);
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void BounceBall(string indexStr)
    {
        int index = -1;
        if (indexStr == "0")
            index = 0;
        else if (indexStr == "1")
            index = 1;
        else if (indexStr == "2")
            index = 2;
        else if (indexStr == "3")
            index = 3;
        else if (indexStr == "4")
            index = 4;

        if(index != -1)
            balls[index].BounceThatBoi();
    }

    public void UpdateState(PluginConnector.GAMESTATE gamestateIn)
    {
        currentState = gamestateIn; //Gets the state from the plugin

        if(prevState == currentState) //This will ensure that entry functions only happen when the state is changed
        {
            //Preforms update for the current state if needed
        }
        else
        {
            prevState = currentState; //Updates the state if it changed
            switch (currentState)
            {
                case PluginConnector.GAMESTATE.MENU:
                    MoveToMainMenu();
                    break;

                case PluginConnector.GAMESTATE.PLAYER:
                    SwapToPickingMode();
                    break;

                case PluginConnector.GAMESTATE.WAITING_PLAYER:
                    //TODO: Plugin this functionality
                    break;

                case PluginConnector.GAMESTATE.CZAR:
                    SwapToCzarMode();
                    break;

                case PluginConnector.GAMESTATE.NULL:
                    //TODO: Error message, this should never be null
                    return;
                    break;
            }
        }


    }

    void SwapToCzarMode()
    {
        TurnOffAllCanvas();
        czarHolder.czarCanvas.enabled = true;

        //Wipe the text of all of the buttons
        foreach (Text text in czarHolder.responceButtonTexts)
            text.text = "???"; 
    }

    void SwapToPickingMode()
    {
        TurnOffAllCanvas();
        playerHolder.playerCanvas.enabled = true;
    }

    void TurnOffAllCanvas()
    {
        foreach(Canvas canvas in allCanvas)
        {
            canvas.enabled = false;
        }
    }

    public void RecivePromptToDisplay(string thePrompt)
    {
        playerHolder.promptDisplay.text = thePrompt; //Display the prompt recived
        czarHolder.promptDisplay.text = thePrompt;
    }

    public void DisplayCzarChoice(string theChoice)
    {
        bool displayed = false;

        foreach(Text text in czarHolder.responceButtonTexts)
        {
            if (text.text != "???" && !displayed)
            {
                text.text = theChoice;
                displayed = true;
            }
        }
    }

    public void RecivePoints()
    {
        points++;
    }

    //BUTTON FUNCTIONS---------------------------------------------------------------------------

    public void SendResponce()
    {
        if(playerHolder.responceText.text != "")
        {
            pluginConnector.SendResponce(playerHolder.responceText.text, userName);
        }
    }

    public void SendChoice(Text inText)
    {
        bool valid = true;

        //Checks to make sure all of the players has given a choice
        foreach(Text text in czarHolder.responceButtonTexts)
        {
            if (text.text == "???")
                valid = false;
        }

        if(valid)
        {
            pluginConnector.SendCzarChoice(inText.text, userName);
        }
    }

    public void OpenConnectingMenu()
    {
        TurnOffAllCanvas();
        wantToConnectCanvas.enabled = true;
    }

    public void ConnectToServer(Text IP) //Sends the connection request and sets the game into a waiting state
    {
        pluginConnector.ConnectToIP(IP.text);
        userName = userNameInput.text;
        currentState = PluginConnector.GAMESTATE.CONNECTING;
    }

    public void MoveToMainMenu()
    {
        TurnOffAllCanvas();
        mainMenuCanvas.enabled = true;
    }
}
