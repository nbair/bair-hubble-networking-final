﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CzarCanvasDataHolder : MonoBehaviour
{
    /*  This script exists mostly for orginzational reasons, exits purely to hold data
     */

    public List<Button> responceButtons = new List<Button>();
    public Canvas czarCanvas;
    public TextMeshProUGUI promptDisplay;

    [HideInInspector]
    public List<Text> responceButtonTexts = new List<Text>();

    private void Start()
    {
        czarCanvas = this.gameObject.GetComponent<Canvas>();

        //This line of code is fucking nasty, but it's all on start so it shouldn't be too bad
        foreach (Button button in responceButtons)
            responceButtonTexts.Add(button.transform.GetChild(0).GetComponent<Text>()); 
    }

}
