﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerCanvasDataHolder : MonoBehaviour
{
    /*  This script exists mostly for orginzational reasons, exits purely to hold data
    */

    public InputField responceText;
    public Canvas playerCanvas;
    public TextMeshProUGUI promptDisplay;

    private void Start()
    {
        playerCanvas = this.gameObject.GetComponent<Canvas>();
    }

}
